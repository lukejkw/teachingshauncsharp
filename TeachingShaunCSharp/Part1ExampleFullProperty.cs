﻿using NUnit.Framework;

namespace TeachingShaunCSharp
{
    public class Part1ExampleFullProperty
    {
        /// <summary>
        ///  Our member variable (private means that we can only access this variable from within this class)
        /// </summary>
        private string _name;

        /// <summary>
        /// This Property always returns our _name variable in uppercase but sets it in lowercase
        /// </summary>
        public string Name
        {
            get
            {
                return _name.ToUpper();
            }
            set
            {
                _name = value.ToLower();
            }
        }

        public Part1ExampleFullProperty()
        {

        }
    }

    /// <summary>
    /// Tests are a way to (you guessed it) TEST our code.
    /// 
    /// We are using NUnit to test our methods (google: "NUnit Test explorer window to see how to run your tests")
    /// </summary>
    [TestFixture]
    public class Part1ExampleFullPropertyTests
    {
        /// <summary>
        /// Don't worry about the test stuff, this method/function just shows the example class in action
        /// </summary>
        [Test]
        public void NameIsReturnedInUpperCase()
        {
            var part1Example = new Part1ExampleFullProperty();

            // Set the property value
            part1Example.Name = "Shaun";

            // We can't do this because _name is private (will not compile)
            //part1Example._name = "Shaun";

            Assert.AreEqual("SHAUN", part1Example.Name);
        }
    }
}