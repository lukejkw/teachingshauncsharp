﻿using NUnit.Framework;

namespace TeachingShaunCSharp
{
    public class Part1ExampleAutoProperty
    {
        /// <summary>
        /// This is an auto property and uses a shorthand get; set;
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// This is an auto property that has a public get but a private set.
        /// 
        /// This means that it can only be set from within the class, like from the contructor
        /// </summary>
        public int UserId { get; private set; }

        public Part1ExampleAutoProperty(int userId)
        {
            UserId = userId;
        }
    }

    [TestFixture]
    public class Part1ExampleAutoPropertyTests
    {
        [Test]
        public void CanSetUserName()
        {
            var part1Auto = new Part1ExampleAutoProperty(1);

            part1Auto.UserName = "Shaun";

            // Can't access setter of UserId
            //part1Auto.UserId = 2;// Cant do this because private set;

            Assert.AreEqual("Shaun", part1Auto.UserName);
            Assert.AreEqual(1, part1Auto.UserId);
        }
    }
}